<?php
//client side change data
$nameChange  = $_POST['changed_name'];
$emailChange = $_POST['changed_email'];
$accountId = $_POST['account_Id'];
$address = $_POST['changed_address'];

//echo $nameChange."<br>";
//echo $emailChange."<br>";
//echo $accountId."<br>";
//echo $address."<br>";

$url = "http://localhost/cosc465/Project3/ChangedUpdate.php";
$curl = curl_init($url);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
curl_setopt($curl, CURLOPT_POSTFIELDS, "changed_name=$nameChange&changed_email=$emailChange&account_Id=$accountId&address=$address");
$response = curl_exec($curl);

$result=json_decode($response);
//var_dump($result);
if($result->status==201){
  $message="Successfully updated data!";
} else {
  $message="Unable to update data";
}
curl_close($curl);
?>
<html>
<head>
  <title>Updating data...</title>
</head>
<body>
  <?php echo $message?>
  <form action="/cosc465/Project3/" method="GET">
    <input type="submit" value="Return to the form">
  </form>
</body>
</html>
