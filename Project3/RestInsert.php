<?php
//client side insertion
$display_name = $_REQUEST['display_name'];
$email = $_REQUEST['email'];
$address = $_REQUEST['address'];
//echo $display_name."<br>";
//echo $email."<br>";
//echo $address."<br>";

$url = "http://localhost/cosc465/Project3/Insertion.php";
$curl = curl_init($url);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
curl_setopt($curl, CURLOPT_POSTFIELDS, "display_name=$display_name&email=$email&address=$address");
$response = curl_exec($curl);
curl_close($curl);
$result=json_decode($response);

//var_dump($result);

if($result->status==201){
  $message="Successfully inserted data!";
} else {
  $message = "Unable to insert data";
}
?>

<html>
  <head>
    <title>Inserting into database...</title>
  </head>
  <body>
    <?php echo $message ?>
    <form action="/cosc465/Project3/" method="GET">
      <input type="submit" value="Return to the form">
    </form>
  </body>
</html>
