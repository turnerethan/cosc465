<?php
//client side delete
$deletedUser = $_POST['deleted_Id'];

$url = "http://localhost/cosc465/Project3/DeleteUpdate.php";
$ch = curl_init($url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_POSTFIELDS, "deleted_Id=$deletedUser");
$response = curl_exec($ch);
curl_close($ch);
$result=json_decode($response);
if($result->status==201){
  $message="Successfully deleted user!";
} else {
  $message="Unable to delete user";
}
?>

<html>
<head>
  <title>Deleting data...</title>
</head>
<body>
  <?php echo $message ?>
  <form action="/cosc465/Project3/", method="GET">
    <input type="submit", value="Return to the form">
  </form>
</body>
</html>
